<?php
    
    $servername = $_GET["servername"];
    $username = $_GET["username"];
    $password = $_GET["password"];
    $dbname = $_GET["dbname"];
    $whereclause = $_GET["where"];
    
    // Create connection
    $conn = mysql_connect($servername, $username, $password);
    if(! $conn )
    {
        die('Could not connect: ' . mysql_error());
    }
    mysql_select_db($dbname);
    
    $sql = "SELECT * FROM tour WHERE " . $whereclause;
    
    $result = mysql_query( $sql, $conn );
    
    if (!$result) {
        echo "Database error " . mysql_error();
    } else {
        $index = 0;
        while ($row = mysql_fetch_assoc($result)){
            $driver[$index] = array('id' => $row['id'],
                          'User_ID' => $row['User_ID'],
                          'Driver_ID' => $row['Driver_ID'],
                          'From_Location' => $row['From_Location'],
                          'To_Location' => $row['To_Location'],
                          'Total_Length' => $row['Total_Length'],
                          'Price' => $row['Price'],
                          'From_Time' => $row['From_Time'],
                          'To_Time' => $row['To_Time'],
                                  );
            $index++;
        }
        echo json_encode($driver);
    }
    mysql_close($conn);
?>