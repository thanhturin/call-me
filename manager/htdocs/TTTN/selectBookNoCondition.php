<?php
    
    $servername = $_GET["servername"];
    $username = $_GET["username"];
    $password = $_GET["password"];
    $dbname = $_GET["dbname"];
    
    // Create connection
    $conn = mysql_connect($servername, $username, $password);
    if(! $conn )
    {
        die('Could not connect: ' . mysql_error());
    }
    mysql_select_db($dbname);
    
    $sql = "SELECT * FROM Book";
    
    $result = mysql_query( $sql, $conn );
    
    if (!$result) {
        echo "Database error " . mysql_error();
    } else {
        $index = 0;
        while ($row = mysql_fetch_assoc($result)){
            $user[$index] = array('ID' => $row['ID'],
                          'FROM_LOCATION' => $row['FROM_LOCATION'],
                          'TO_LOCATION' => $row['TO_LOCATION'],
                          'SEATS' => $row['SEATS'],
                          'NUMBER_CUSTOMER' => $row['NUMBER_CUSTOMER'],
                          'DATE_GO' => $row['DATE_GO'],
                          'TIME_GO' => $row['TIME_GO'],
                          'CUSTOMER_PHONE' => $row['CUSTOMER_PHONE'],
                          'DRIVER_PHONE' => $row['DRIVER_PHONE'],
                          'MONEY' => $row['MONEY'],
                          'STATE' => $row['STATE']
                                  );
            $index++;
        }
        echo json_encode($user);
    }
    mysql_close($conn);
?>