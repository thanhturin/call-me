<?php
    
    $servername = $_GET["servername"];
    $username = $_GET["username"];
    $password = $_GET["password"];
    $dbname = $_GET["dbname"];
    
    // Create connection
    $conn = mysql_connect($servername, $username, $password);
    if(! $conn )
    {
        die('Could not connect: ' . mysql_error());
    }
    mysql_select_db($dbname);
    
    $sql = "SELECT * FROM driverinfo";
    
    $result = mysql_query( $sql, $conn );
    
    if (!$result) {
        echo "Database error " . mysql_error();
    } else {
        $index = 0;
        while ($row = mysql_fetch_assoc($result)){
            $driver[$index] = array('Name' => $row['Name'],
                          'Pass' => $row['Pass'],
                          'Email' => $row['Email'],
                          'Phone' => $row['Phone'],
                          'Address' => $row['Address'],
                          'BirthDay' => $row['BirthDay'],
                          'Sex' => $row['Sex'],
                          'Longitude' => $row['Longitude'],
                          'Latitude' => $row['Latitude'],
                          'IS_IDLE' => $row['IS_IDLE'],
                          'CarID' => $row['CarID'],
                          'Company' => $row['Company'],
                          'IPAddress' => $row['IPAddress'],
                                  );
            $index++;
        }
        echo json_encode($driver);
    }
    mysql_close($conn);
?>