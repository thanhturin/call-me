<?php
	include('db.php');
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Charts | Creative - Bootstrap 3 Responsive Admin Template</title>

    <!-- Bootstrap CSS -->    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="css/elegant-icons-style.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />    
    <!-- Custom styles -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />

    <script src="assets/chart-master/Chart.js"></script>
    <style>
          canvas{
          }
        </style>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->
  </head>

  <body>
  <!-- container section start -->
  <section id="container" class="">
      <!--header start-->
      <header class="header dark-bg">
            <div class="toggle-nav">
                <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
            </div>

            <!--logo start-->
            <a href="index.html" class="logo">Vinasun <span class="lite">Admin</span></a>
            <!--logo end-->

            <div class="top-nav notification-row">                
                <!-- notificatoin dropdown start-->
                <ul class="nav pull-right top-menu">
                    <!-- alert notification end-->
                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="profile-ava">
                                <img alt="" src="img/avatar1_small.jpg">
                            </span>
                            <span class="username">Tạ Phúc Thành</span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>
                            <li class="eborder-top">
                                <a href="#"><i class="icon_profile"></i> My Profile</a>
                            </li>
                            <li>
                                <a href="#"><i class="icon_mail_alt"></i> My Inbox</a>
                            </li>
                            <li>
                                <a href="#"><i class="icon_clock_alt"></i> Timeline</a>
                            </li>
                            <li>
                                <a href="#"><i class="icon_chat_alt"></i> Chats</a>
                            </li>
                            <li>
                                <a href="login.html"><i class="icon_key_alt"></i> Log Out</a>
                            </li>
                            <li>
                                <a href="documentation.html"><i class="icon_key_alt"></i> Documentation</a>
                            </li>
                            <li>
                                <a href="documentation.html"><i class="icon_key_alt"></i> Documentation</a>
                            </li>
                        </ul>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!-- notificatoin dropdown end-->
            </div>
      </header>      
      <!--header end-->

      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu">                
                  <li class="">
                      <a class="" href="table_maps.html">
                          <i class="icon_house_alt"></i>
                          <span>Trang chủ</span>
                      </a>
                  </li>
                             
                  <li class="sub-menu">
                      <a href="table.php" class="">
                          <i class="icon_table"></i>
                          <span>Danh sách tài xế</span>
                      </a>
                  </li>
                  
                  <li class="sub-menu">
                      <a href="chart.php" class="">
                          <i class="icon_piechart"></i>
                          <span>Thống kê</span>
                      </a>
                  </li>
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>

      <!--main content start-->      
      <section id="main-content">
        <section class="wrapper">
		<div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="icon_piechart"></i> Thống kê</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="index.html">Trang chủ</a></li>
						<li><i class="icon_piechart"></i>Thống kê trong tuần</li>
					</ol>
				</div>
			</div>
            <div class="row">
              <!-- chart morris start -->
              <div class="col-lg-12">
                  <section class="panel">
                      <div class="btn-group">
                                                  <a class="btn btn-default" href="" title="Bootstrap 3 themes generator">Thống kê</a>
                                                  <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="" title="Bootstrap 3 themes generator"><span class="caret"></span></a>
                                                  <ul class="dropdown-menu">
                                                    <li><a href="chart_week.php" title="Bootstrap 3 themes generator">Thống kê trong tuần</a></li>
                                                    <li><a href="chart_month.php" title="Bootstrap 3 themes generator">Thống kê trong tháng</a></li>
                                                    <li><a href="chart_year.php" title="Bootstrap 3 themes generator">Thống kê trong năm</a></li>
                                                  </ul>
                                        </div><!-- /btn-group -->
                      <div class="panel-body">
                        <div class="tab-pane" id="chartjs">
<?php
    date_default_timezone_set('Asia/Saigon');
    $days = array();
    for ($i=1; $i < 32; $i++) { 
      array_push($days, $i);
    }
    $month = date("m");
    $last_month = date("m", strtotime("-1 months"));

    $money_week = array();
    $money_last_week = array();
    mysql_query("SET NAMES 'UTF8'");
    // this week
        for ($i=0; $i < count($month); $i++) {
          $getselect = mysql_query("SELECT SUM(MONEY) AS sum_money FROM Book WHERE COMPANY_NAME = 'Vinasun' AND DAY(DATE_GO) = '$days[$i]' AND MONTH(DATE_GO) = '$month' AND STATE = 'Finish'");
           $rowcount = mysql_num_rows($getselect);
           if ($rowcount == 0) {
              array_push($money_week, '0');
           }
           else {
                while($bookrow = mysql_fetch_array($getselect))
                {
                  $money = $bookrow['sum_money'];
                  array_push($money_week, $money);
                }
           } 
        }

        // previous week
        for ($i=0; $i < count($month); $i++) { 
            $getselect = mysql_query("SELECT SUM(MONEY) AS sum_money FROM Book WHERE COMPANY_NAME = 'Vinasun' AND DAY(DATE_GO) = '$days[$i]' AND MONTH(DATE_GO) = '$last_month' AND STATE = 'Finish'");
           $rowcount = mysql_num_rows($getselect);
           if ($rowcount == 0) {
              array_push($money_last_week, '0');
           }
           else {
                while($bookrow = mysql_fetch_array($getselect))
                {
                  $money = $bookrow['sum_money'];
                  array_push($money_last_week, $money);
                }
           }
        }
        
?>                       
                            <div class="panel-body text-center">
                                <canvas id="canvas" height="450" width="600"></canvas>
                                    <script>
                                    	  var barChartLabels = <?php echo json_encode($days); ?>;
                                        var this_week_data = <?php echo json_encode($money_week); ?>;
                                        var last_week_data = <?php echo json_encode($money_last_week); ?>;
                                        var barChartData = {
                                            labels : barChartLabels,
                                            datasets : [
                                                      {
                                                        fillColor : "rgba(220,220,220,0.5)",
                                                        strokeColor : "rgba(220,220,220,1)",
                                                        data : last_week_data
                                                      },
                                                      {
                                                        fillColor : "rgba(151,187,205,0.5)",
                                                        strokeColor : "rgba(151,187,205,1)",
                                                        data : this_week_data
                                                      }
                                                    ]
                                                    
                                                  }
                                        var myLine = new Chart(document.getElementById("canvas").getContext("2d")).Bar(barChartData);
                                    </script>   
                            </div>
<?php ?>                            
                        </div>
                      </div>
                    </section>
              </div>
              <!-- chart morris start -->
            </div>
      </section>
      <!--main content end-->
    </section>
    <!-- container section end -->
    <!-- javascripts -->
    <script src="js/jquery.js"></script>
    <script src="js/jquery-1.8.3.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
    <!-- chartjs -->
    <script src="assets/chart-master/Chart.js"></script>
    <!-- custom chart script for this page only-->
    <script src="js/chartjs-custom.js"></script>
    <!--custome script for all page-->
    <script src="js/scripts.js"></script>

  </body>
</html>
