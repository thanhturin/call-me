<?php 
ob_start();
include('db.php');
if(isset($_GET['id']))
{
  $id=$_GET['id'];
  if(isset($_POST['update']))
  {
    $newname=$_POST['newname'];
    $newbirthday=$_POST['newbirthday'];
    $newphone=$_POST['newphone'];
    $newsex=$_POST['newsex'];
    $newaddress=$_POST['newaddress'];
    $newemail=$_POST['newemail'];

    $updated=mysql_query("UPDATE driverinfo SET 
      Name='$newname', BirthDay='$newbirthday', Phone='$newphone', Sex = '$newsex', Address = '$newaddress', Email = '$newemail' WHERE Phone='$id'") or die();
    if($updated)
    {
      $msg="Successfully Updated!!";
      header('Location:table.php');
    }
  }
}
ob_end_flush();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Thông tin tài xế</title>

    <!-- Bootstrap CSS -->    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="css/elegant-icons-style.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />

    <script src="assets/chart-master/Chart.js"></script>
    <style>
          canvas{
          }
        </style>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->

  </head>

  <body onload="load()">

  <!-- container section start -->
  <section id="container" class="">
      <!--header start-->
      <header class="header dark-bg">
            <div class="toggle-nav">
                <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
            </div>

            <!--logo start-->
            <a href="index.html" class="logo">Vinasun <span class="lite">Admin</span></a>
            <!--logo end-->

            <div class="top-nav notification-row">                
                <!-- notificatoin dropdown start-->
                <ul class="nav pull-right top-menu">
                    <!-- alert notification end-->
                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="profile-ava">
                                <img alt="" src="img/avatar1_small.jpg">
                            </span>
                            <span class="username">Tạ Phúc Thành</span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>
                            <li class="eborder-top">
                                <a href="#"><i class="icon_profile"></i> My Profile</a>
                            </li>
                            <li>
                                <a href="#"><i class="icon_mail_alt"></i> My Inbox</a>
                            </li>
                            <li>
                                <a href="#"><i class="icon_clock_alt"></i> Timeline</a>
                            </li>
                            <li>
                                <a href="#"><i class="icon_chat_alt"></i> Chats</a>
                            </li>
                            <li>
                                <a href="login.html"><i class="icon_key_alt"></i> Log Out</a>
                            </li>
                            <li>
                                <a href="documentation.html"><i class="icon_key_alt"></i> Documentation</a>
                            </li>
                            <li>
                                <a href="documentation.html"><i class="icon_key_alt"></i> Documentation</a>
                            </li>
                        </ul>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!-- notificatoin dropdown end-->
            </div>
      </header>      
      <!--header end-->

      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu">                
                  <li class="">
                      <a class="" href="table_maps.html">
                          <i class="icon_house_alt"></i>
                          <span>Trang chủ</span>
                      </a>
                  </li>
                             
                  <li class="sub-menu">
                      <a href="table.php" class="">
                          <i class="icon_table"></i>
                          <span>Danh sách tài xế</span>
                      </a>
                  </li>
                  
                  <li class="sub-menu">
                      <a href="chart.php" class="">
                          <i class="icon_piechart"></i>
                          <span>Thống kê</span>
                      </a>
                  </li>
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>

      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              
              <!-- page start-->
              <div class="row">
                 <div class="col-lg-12">
                    <section class="panel">
                          <header class="panel-heading tab-bg-info">
                              <ul class="nav nav-tabs">
                                  <li class="active">
                                      <a data-toggle="tab" href="#profile">
                                          <i class="icon-user"></i>
                                          Profile
                                      </a>
                                  </li>
                                  <li class="">
                                      <a data-toggle="tab" href="#edit-profile">
                                          <i class="icon-envelope"></i>
                                          Edit Profile
                                      </a>
                                  </li>
                              </ul>
                          </header>
                          <div class="panel-body">
                              <div class="tab-content">
                                  <!-- profile -->
<?php
    date_default_timezone_set('Asia/Saigon');

    $month = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
    $month_label = array("Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12");

    $year = date("Y");
    $last_year = date("Y", strtotime("-1 years"));

    $money_week = array();
    $money_last_week = array();
    mysql_query("SET NAMES 'UTF8'");

    if(isset($_GET['id']))
    {
        $id=$_GET['id'];
        
        // this year
        for ($i=0; $i < count($month); $i++) { 
           $getselect = mysql_query("SELECT SUM(MONEY) AS sum_money FROM Book WHERE DRIVER_PHONE = '$id' AND MONTH(DATE_GO) = '$month[$i]' AND YEAR(DATE_GO) = '$year' AND STATE = 'Finish'");

           $rowcount = mysql_num_rows($getselect);
           if ($rowcount == 0) {
              array_push($money_week, '0');
           }
           else {
                while($bookrow = mysql_fetch_array($getselect))
                {
                  $money = $bookrow['sum_money'];
                  array_push($money_week, $money);
                }
           }
        }

        // last year
        for ($i=0; $i < count($month); $i++) { 
           $getselect = mysql_query("SELECT SUM(MONEY) AS sum_money FROM Book WHERE DRIVER_PHONE = '$id' AND Month(DATE_GO) = '$month[$i]' AND YEAR(DATE_GO) = '$last_year' AND STATE = 'Finish'");
           $rowcount = mysql_num_rows($getselect);
           if ($rowcount == 0) {
              array_push($money_last_week, '0');
           }
           else {
                while($bookrow = mysql_fetch_array($getselect))
                {
                  $money = $bookrow['sum_money'];
                  array_push($money_last_week, $money);
                }
           }
        }

        $getselect=mysql_query("SELECT * FROM driverinfo WHERE Phone='$id'");
        while($userrow=mysql_fetch_array($getselect))
        {
            $name = $userrow['Name'];
            $birthday = $userrow['BirthDay'];
            $phone = $userrow['Phone'];
            $sex = $userrow['Sex'];
            $address = $userrow['Address'];
            $email = $userrow['Email'];
            $carID = $userrow['CarID'];
            ?>
                                  <div id="profile" class="tab-pane active">
                                    <section class="panel">
                                      <div class="panel-body bio-graph-info">
                                          <h1>Thông tin cá nhân</h1>
                                          <div class="row">
                                              <div class="bio-row">
                                                  <p><span>First Name </span>: <?php echo "$name"; ?> </p>
                                              </div>
                                              <div class="bio-row">
                                                  <p><span>Birthday</span>: <?php echo "$birthday"; ?></p>
                                              </div>
                                              <div class="bio-row">
                                                  <p><span>Address </span>: <?php echo "$address"; ?></p>
                                              </div>
                                              <div class="bio-row">
                                                  <p><span>Email </span>: <?php echo "$email"; ?> </p>
                                              </div>
                                              <div class="bio-row">
                                                  <p><span>Mobile </span>: <?php echo "$phone"; ?> </p>
                                              </div>
                                              <div class="bio-row">
                                                  <p><span>Car ID </span>: <?php echo "$carID"; ?></p>
                                              </div class="bio-row">
                                          </div>
                                      </div>

                                      <div class="panel-body bio-graph-info">
                                        <div class="btn-group">
                                                  <a class="btn btn-default" href="" title="Bootstrap 3 themes generator">Thống kê</a>
                                                  <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="" title="Bootstrap 3 themes generator"><span class="caret"></span></a>
                                                  <ul class="dropdown-menu">
                                                    <li><a href="table_profile_week.php?id=<?php echo $id; ?>" title="Bootstrap 3 themes generator">Thống kê trong tuần</a></li>
                                                    <li><a href="table_profile_month.php?id=<?php echo $id; ?>" title="Bootstrap 3 themes generator">Thống kê trong tháng</a></li>
                                                    <li><a href="table_profile_year.php?id=<?php echo $id; ?>" title="Bootstrap 3 themes generator">Thống kê trong năm</a></li>
                                                  </ul>
                                            </div><!-- /btn-group -->
                                         <div class="panel-body text-center">
                                            <canvas id="canvas" height="450" width="900"></canvas>
                                                <script>
                                                var barChartLabels = <?php echo json_encode($month_label); ?>;
                                                var this_week_data = <?php echo json_encode($money_week); ?>;
                                                var last_week_data = <?php echo json_encode($money_last_week); ?>;
                                                var barChartData = {
                                                    labels : barChartLabels,
                                                    datasets : [
                                                      {
                                                        fillColor : "rgba(220,220,220,0.5)",
                                                        strokeColor : "rgba(220,220,220,1)",
                                                        data : last_week_data
                                                      },
                                                      {
                                                        fillColor : "rgba(151,187,205,0.5)",
                                                        strokeColor : "rgba(151,187,205,1)",
                                                        data : this_week_data
                                                      }
                                                    ]
                                                    
                                                  }
                                                var myLine = new Chart(document.getElementById("canvas").getContext("2d")).Bar(barChartData);
                                                </script>   
                                          </div>
                                      </div>
                                    </section>
                                  </div>
                                  <!-- edit-profile -->
                                  <div id="edit-profile" class="tab-pane">

                                    <section class="panel">                                          
                                          <div class="panel-body bio-graph-info">
                                              <h1> Profile Info</h1>
                                              <form class="form-horizontal" role="form" action="" method="post" name="insertform">                                                  
                                                  <div class="form-group">
                                                      <label class="col-lg-2 control-label">Name</label>
                                                      <div class="col-lg-6">
                                                          <input type="text" class="form-control" name="newname" required placeholder="Họ và tên" 
        value="<?php echo $name; ?>" id="inputid">
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-lg-2 control-label">Birthday</label>
                                                      <div class="col-lg-6">
                                                          <input type="text" class="form-control" name="newbirthday" required placeholder="Ngày sinh (dd/mm/yyyy)" 
        value="<?php echo $birthday; ?>" id="inputid">
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-lg-2 control-label">Address</label>
                                                      <div class="col-lg-6">
                                                          <input type="text" class="form-control" name="newaddress" required placeholder="Địa chỉ" 
        value="<?php echo $address; ?>" id="inputid">
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-lg-2 control-label">Email</label>
                                                      <div class="col-lg-6">
                                                          <input type="text" class="form-control" name="newemail" required placeholder="Địa chỉ email" 
        value="<?php echo $email; ?>" id="inputid">
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-lg-2 control-label">Mobile</label>
                                                      <div class="col-lg-6">
                                                          <input type="text" class="form-control" name="newphone" required placeholder="Số điện thoại" 
        value="<?php echo $phone; ?>" id="inputid">
                                                      </div>
                                                  </div>

                                                  <div class="form-group">
                                                      <div class="col-lg-offset-2 col-lg-10">
                                                          <button type="submit" class="btn btn-primary" name="update" value="Update" id="inputid">Save</button>
                                                          <button type="button" class="btn btn-danger">Cancel</button>
                                                      </div>
                                                  </div>

                                              </form>
                                          </div>
                                      </section>
<?php } } ?>
                                  </div>
                              </div>
                          </div>
                      </section>
                 </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
  </section>
  <!-- container section end -->
    <!-- javascripts -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- nicescroll -->
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
    <!--custome script for all page-->
    <script src="js/scripts.js"></script>


  </body>
</html>